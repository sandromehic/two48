# Two48

Programming challenge to develop 2048 game with cooperation mode. Developed with
`phoenix`, `vue` and `tailwindcss`.

## How to run

Three possibilities:

- `./run.sh` - downloads the docker image from gitlab registry and runs it
- `./run_dockerbuild.sh` - builds the docker image locally and runs it
- `./run_phoenix.sh` - get backend and frontend dependencies and run phoenix server

All three scripts start the server on [http://localhost:4000](http://localhost:4000)
