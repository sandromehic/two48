import '../css/app.scss';

import 'phoenix_html';

import Vue from 'vue';
import App from './components/App.vue';
import Game from './components/Game.vue';
import Board from './components/Board.vue';
import Chat from './components/Chat.vue';

Vue.component('game', Game);
Vue.component('board', Board);
Vue.component('chat', Chat);

new Vue({
  render: (h) => h(App),
}).$mount('#vue-app');
