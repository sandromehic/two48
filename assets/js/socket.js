import { Socket } from 'phoenix';

function scrollChatBox() {
  const element = document.getElementById('chatbox');
  element.scrollTop = element.scrollHeight;
}

const socket = {
  state: {
    socket: null,
    channel: null,
    board: null,
    error: null,
    messages: [],
    connected: false,
    username: '',
  },
  pushEvent(event, payload) {
    this.state.channel.push(event, payload);
  },
  sendMessage(msg) {
    this.pushEvent('message', { body: msg, username: this.state.username });
  },
  updateBoard(board) {
    this.state.board = board;
  },
  updateMessages(newMessage) {
    this.state.messages.push(newMessage);
    setTimeout(scrollChatBox, 0);
  },
  join(username) {
    this.state.socket = new Socket('/socket', { params: { username } });
    this.state.channel = this.state.socket.channel('game', { });
    this.state.channel.join()
      .receive('ok', (payload) => { this.onJoin(payload); })
      .receive('error', (resp) => { this.state.error = resp; });

    this.state.channel.on('move', (payload) => this.updateBoard(payload));
    this.state.channel.on('message', (payload) => this.updateMessages(payload));
    this.state.socket.connect();
  },
  onJoin(payload) {
    this.state.connected = true;
    this.updateBoard(payload.board);
    this.state.username = payload.username;
  },
};

export { socket as default };
