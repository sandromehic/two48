defmodule Two48.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      # Two48.Repo,
      # Start the Telemetry supervisor
      Two48Web.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Two48.PubSub},
      # Start the Endpoint (http/https)
      Two48Web.Endpoint,
      # Start a worker by calling: Two48.Worker.start_link(arg)
      # {Two48.Worker, arg}
      Two48.Game
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Two48.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Two48Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
