defmodule Two48.Game do
  @moduledoc """
  Main context for the game and a stateful server. Responsible for:

  - Holding the board state and integrating with `Board` module
  - Receive player commands and execute them against the board
  """

  @pid __MODULE__

  @doc """
  Returns the current board
  """
  def board(pid \\ @pid) do
    GenServer.call(pid, :board)
  end

  @doc """
  Moves the board
  """
  def move(pid \\ @pid, direction) do
    GenServer.call(pid, {:move, direction})
  end

  @doc """
  Reset the current board and starts the game over.
  """
  def reset(params, pid \\ @pid) do
    GenServer.call(pid, {:reset, reset_attrs(params)})
  end

  defp reset_attrs(%{"obstacles" => value}) when is_integer(value), do: %{obstacles: value}

  defp reset_attrs(%{"obstacles" => value}) when is_binary(value),
    do: %{obstacles: String.to_integer(value)}

  defp reset_attrs(_), do: %{}

  # Server code

  alias Two48.Game.Board

  use GenServer

  def start_link(opts) do
    name = opts[:name] || @pid
    GenServer.start_link(@pid, opts, name: name)
  end

  @impl true
  def init(opts) do
    # allow board to be passed for testing the server
    if opts[:board] do
      {:ok, opts[:board]}
    else
      Board.new()
    end
  end

  @impl true
  def handle_call(:board, _from, board) do
    {:reply, board, board}
  end

  def handle_call({:move, direction}, _from, board) do
    {response, updated_board} =
      case Board.move(board, direction) do
        {:ok, moved} -> {{:ok, moved}, moved}
        {:error, reason} -> {{:error, reason}, board}
      end

    {:reply, response, updated_board}
  end

  def handle_call({:reset, attrs}, _from, _board) do
    {:ok, board} = Board.new(attrs)

    {:reply, board, board}
  end
end
