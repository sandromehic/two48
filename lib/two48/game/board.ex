defmodule Two48.Game.Board do
  @moduledoc """
  Module responsible for the business logic of creating and moving game board.
  """

  @size 6
  @new_value 2
  @obstacle_value "x"
  @valid_values [0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048] ++ [@obstacle_value]

  alias Two48.Game.Board
  alias Two48.Game.BoardUtils

  @type t :: %__MODULE__{
          tiles: list(),
          size: integer(),
          won: boolean(),
          lost: boolean(),
          obstacles: integer()
        }

  @derive Jason.Encoder
  defstruct [:tiles, size: @size, won: false, lost: false, obstacles: 0]

  @doc """
  Creates a new board for a fresh game.
  """
  @spec new(map()) :: {:ok, Board.t()}
  def new(attrs \\ %{}) do
    tiles = BoardUtils.zeros(@size * @size)
    obstacles = Map.get(attrs, :obstacles, 0)

    board =
      %Board{tiles: tiles, obstacles: obstacles}
      |> generate_obstacles(obstacles)
      |> new_tile()

    {:ok, board}
  end

  defp generate_obstacles(board, 0), do: board

  defp generate_obstacles(board, n) when n > 0 do
    board |> new_tile(@obstacle_value) |> generate_obstacles(n - 1)
  end

  @doc """
  Create a board from a list of tiles. Accepts optional arguments describing the
  structure of the tiles. Options can be:

  - `columns`: when set to `true` the tiles will be transposed
  """
  @spec from(any(), list()) :: {:ok, Board.t()} | {:error, String.t()}
  def from(data, opts \\ [])

  def from(data, opts) when is_list(data) do
    with tiles <- List.flatten(data),
         {:ok, _} <- BoardUtils.members?(tiles, @valid_values),
         {:ok, size} <- BoardUtils.square_size(tiles),
         {:ok, tiles} <- from_columns(tiles, opts[:columns]) do
      {:ok, %Board{tiles: tiles, size: size, obstacles: n_obstacles(tiles)}}
    else
      {:error, reason} -> {:error, reason}
      _ -> {:error, "unknown error"}
    end
  end

  def from(_, _), do: {:error, "please provide a list of tiles"}

  defp from_columns(tiles, true), do: {:ok, BoardUtils.transpose!(tiles)}
  defp from_columns(tiles, _), do: {:ok, tiles}

  defp n_obstacles(tiles) do
    tiles
    |> Enum.filter(fn t -> t == @obstacle_value end)
    |> Enum.count()
  end

  @doc """
  Main function for playing the game. `move/2` checks whether the board is already
  finished or unmovable. When possible `move/2` will slide the board in the
  `direction` specified.
  """
  @spec move(Board.t(), atom()) :: {:ok, Board.t()} | {:error, String.t()}
  def move(%{won: true}, _direction), do: {:error, "game is already won"}
  def move(%{lost: true}, _direction), do: {:error, "game is already lost"}

  def move(board, direction) do
    case move_and_collapse(board, direction) do
      {:ok, collapsed} ->
        moved =
          collapsed
          |> win_condition()
          |> new_tile()
          |> lose_condition()

        {:ok, moved}

      {:error, message} ->
        {:error, message}
    end
  end

  defp move_and_collapse(board, direction) do
    collapsed = collapse(board, direction)

    if collapsed == board do
      {:error, "not movable in #{direction} direction"}
    else
      {:ok, collapsed}
    end
  end

  defp win_condition(board = %{tiles: tiles}) do
    if Enum.any?(tiles, &(&1 == 2048)) do
      %{board | won: true}
    else
      board
    end
  end

  def lose_condition(board = %{tiles: tiles}) do
    if Enum.all?(tiles, &(&1 != 0)) do
      %{board | lost: true}
    else
      board
    end
  end

  @doc """
  Adds a new tile to a random empty tile
  """
  @spec new_tile(Board.t(), any()) :: Board.t()
  def new_tile(board, value \\ @new_value) do
    case random_zero_index(board.tiles) do
      :error ->
        board

      index ->
        tiles = List.update_at(board.tiles, index, fn _ -> value end)
        %{board | tiles: tiles}
    end
  end

  defp random_zero_index(tiles) do
    indexes =
      tiles
      |> Enum.with_index()
      |> Enum.filter(fn {tile, _index} -> tile == 0 end)
      |> Enum.map(fn {_, index} -> index end)

    case indexes do
      [] -> :error
      list -> Enum.random(list)
    end
  end

  @doc """
  Split the board tiles by the rows.
  """
  @spec split_by_rows(Board.t()) :: Board.t()
  def split_by_rows(board = %{tiles: tiles, size: size}) do
    %{board | tiles: Enum.chunk_every(tiles, size)}
  end

  @doc """
  Split the board tiles by columns.
  """
  @spec split_by_columns(Board.t()) :: Board.t()
  def split_by_columns(board = %{tiles: tiles}) do
    board
    |> Map.put(:tiles, BoardUtils.transpose!(tiles))
    |> split_by_rows()
  end

  @doc """
  Splits based on the direction we want to reduce the board in.
  """
  @spec split_by_direction(Board.t(), atom()) :: Board.t()
  def split_by_direction(board, :left) do
    split_by_rows(board) |> split_by_obstacles()
  end

  def split_by_direction(board, :right) do
    split_by_rows(board) |> reverse_tiles() |> split_by_obstacles()
  end

  def split_by_direction(board, :up) do
    split_by_columns(board) |> split_by_obstacles()
  end

  def split_by_direction(board, :down) do
    split_by_columns(board) |> reverse_tiles() |> split_by_obstacles()
  end

  defp split_by_obstacles(board = %{obstacles: 0}), do: board

  defp split_by_obstacles(board = %{tiles: tiles}) do
    tiles =
      Enum.reduce(tiles, [], fn tile_row, acc ->
        BoardUtils.disect_reverse(tile_row, @obstacle_value) ++ acc
      end)
      |> Enum.reverse()

    %{board | tiles: tiles}
  end

  defp reverse_tiles(board = %{tiles: tiles}) do
    tiles = Enum.map(tiles, &Enum.reverse/1)

    %{board | tiles: tiles}
  end

  @doc """
  Collapse the entire board in a `direction`.
  """
  @spec collapse(Board.t(), atom()) :: Board.t()
  def collapse(board, direction) do
    board
    |> split_by_direction(direction)
    |> reduce_tiles()
    |> combine_by_direction(direction)
  end

  defp reduce_tiles(board = %{tiles: tiles}) do
    tiles = Enum.map(tiles, &BoardUtils.collapse/1)
    %{board | tiles: tiles}
  end

  defp combine_by_direction(board, :left) do
    flatten(board)
  end

  defp combine_by_direction(board, :right) do
    board
    |> flatten()
    |> split_by_rows()
    |> reverse_tiles()
    |> flatten()
  end

  defp combine_by_direction(board, :up) do
    board |> flatten() |> transpose()
  end

  defp combine_by_direction(board, :down) do
    board
    |> flatten()
    |> split_by_rows()
    |> reverse_tiles()
    |> flatten()
    |> transpose()
  end

  defp flatten(board = %{tiles: tiles}) do
    %{board | tiles: List.flatten(tiles)}
  end

  defp transpose(board = %{tiles: tiles}) do
    %{board | tiles: BoardUtils.transpose!(tiles)}
  end
end
