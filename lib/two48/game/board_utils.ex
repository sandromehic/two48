defmodule Two48.Game.BoardUtils do
  @moduledoc """
  Board utilities functions
  """

  @spec zeros(integer()) :: list()
  def zeros(0), do: []
  def zeros(n), do: for(_ <- 1..n, do: 0)

  @doc """
  Check if every element of `data` is a member of `values`.
  """
  @spec members?(list(), list()) :: {:ok, list()} | {:error, String.t()}
  def members?([], _values), do: {:error, "no data"}

  def members?(data, values) do
    data
    |> Enum.all?(&Enum.member?(values, &1))
    |> case do
      true -> {:ok, data}
      _ -> {:error, "values not valid"}
    end
  end

  @doc """
  Check if the list has square size and returns it.
  """
  @spec square_size(list()) :: {:ok, integer()} | {:error, String.t()}
  def square_size([]), do: {:error, "bad size"}

  def square_size(data) do
    data
    |> length()
    |> :math.sqrt()
    |> Float.to_string()
    |> Integer.parse()
    |> case do
      {size, ".0"} -> {:ok, size}
      _ -> {:error, "bad size"}
    end
  end

  @doc """
  Returns the square size of the list. Raises an `ArgumentError` if the list
  does not have square size.
  """
  @spec square_size!(list()) :: integer()
  def square_size!(data) do
    case square_size(data) do
      {:ok, size} -> size
      {:error, message} -> raise ArgumentError, message: message
    end
  end

  @doc """
  Given a list representing a matrix, return the list that was transposed.
  """
  @spec transpose(list()) :: {:ok, list()} | {:error, String.t()}
  def transpose(data) when is_list(data) do
    case square_size(data) do
      {:ok, size} -> {:ok, _transpose(data, size)}
      error -> error
    end
  end

  @doc """
  Tries to transpose a list representing a matrix. Raises `ArgumentError` if the
  list is not square.
  """
  @spec transpose!(list()) :: list()
  def transpose!(data) do
    _transpose(data, square_size!(data))
  end

  defp _transpose(data, size) do
    acc = for i <- 0..(size - 1), do: {i, []}
    acc = Enum.into(acc, %{})

    _transpose(data, size, 0, acc)
  end

  defp _transpose([], _, _, acc) do
    acc
    |> Enum.map(fn {_i, col} -> Enum.reverse(col) end)
    |> List.flatten()
  end

  defp _transpose([head | tail], size, index, acc) do
    acc = Map.update!(acc, index, fn list -> [head | list] end)
    _transpose(tail, size, rem(index + 1, size), acc)
  end

  @doc """
  Reduce the list while summing the equal and adjacent values. Fills the final
  list with zeros to make it the same length.
  """
  @spec collapse(list()) :: list()
  def collapse(list) when is_list(list) do
    size = length(list)
    values = list |> Enum.reject(fn v -> v == 0 end)
    collapsed = _collapse(values, [])

    collapsed ++ zeros(size - length(collapsed))
  end

  defp _collapse([], acc), do: Enum.reverse(acc)
  defp _collapse([last], acc), do: _collapse([], [last | acc])

  defp _collapse([first, second | tail], acc) do
    if first == second do
      _collapse(tail, [first * 2 | acc])
    else
      _collapse([second | tail], [first | acc])
    end
  end

  @doc """
  Disects the list based on an element.

  Example:

      iex> disect([1,2,3,2,1], 3)
      [[1, 2], [3], [2, 1]]
  """
  @spec disect(list(), any()) :: list()
  def disect([], _value), do: []

  def disect(data, value) do
    _disect(data, value, [[]])
  end

  @doc """
  Disects the list but returns a reversed accumulato
  """
  def disect_reverse(data, value) do
    _disect(data, value, [[]]) |> Enum.reverse()
  end

  defp _disect([], _, acc) do
    acc
    |> Enum.reject(&Enum.empty?/1)
    |> Enum.map(&Enum.reverse/1)
    |> Enum.reverse()
  end

  defp _disect([value | tail], value, acc) do
    _disect(tail, value, [[] | [[value] | acc]])
  end

  defp _disect([head | tail], value, [acc_head | acc_rest]) do
    acc_head = [head | acc_head]
    _disect(tail, value, [acc_head | acc_rest])
  end
end
