defmodule Two48.Repo do
  use Ecto.Repo,
    otp_app: :two48,
    adapter: Ecto.Adapters.Postgres
end
