defmodule Two48Web.GameChannel do
  @moduledoc """
  Channel that is used for sending game commands to and from clients.
  """

  use Phoenix.Channel

  alias Two48.Game

  def join("game", _, socket) do
    board = Game.board()
    username = socket.assigns[:username]

    send(self(), {:after_join, username})
    {:ok, %{board: board, username: username}, socket}
  end

  def handle_info({:after_join, username}, socket) do
    broadcast!(socket, "message", %{username: username, body: "joined the game!"})
    {:noreply, socket}
  end

  def handle_in("move", %{"direction" => direction}, socket) do
    case cast_direction(direction) do
      :error ->
        {:reply, {:error, %{message: "invalid direction"}}}

      direction ->
        case Game.move(direction) do
          {:ok, board} ->
            broadcast!(socket, "move", board)
            {:noreply, socket}

          {:error, _} ->
            {:noreply, socket}
        end
    end
  end

  def handle_in("message", payload, socket) do
    broadcast!(socket, "message", payload)
    {:noreply, socket}
  end

  def handle_in("reset", params, socket) do
    board = Game.reset(params)

    broadcast!(socket, "move", board)
    {:noreply, socket}
  end

  defp cast_direction("up"), do: :up
  defp cast_direction("down"), do: :down
  defp cast_direction("left"), do: :left
  defp cast_direction("right"), do: :right
  defp cast_direction(_), do: :error
end
