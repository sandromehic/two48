defmodule Two48Web.PageController do
  use Two48Web, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
