#!/bin/bash

set -e

if ! docker -v &> /dev/null
then
    echo "docker required for this script to run"
    echo "if you don't have docker consider running ./run_phoenix.sh"
    exit
fi

IMAGE=registry.gitlab.com/sandromehic/two48:latest

docker run --rm -p 4000:4000 $IMAGE
