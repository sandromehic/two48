#!/bin/bash

if ! mix -v &> /dev/null
then
    echo "elixir required for this script to run"
    echo "if you don't have elixir consider running ./run.sh"
    exit
fi

export SECRET_KEY_BASE=cookie_monster
export MIX_ENV=prod

mix do deps.get, deps.compile
npm --prefix ./assets ci --progress=false --no-audit --loglevel=error
npm --prefix ./assets run deploy
mix phx.digest
mix phx.server
