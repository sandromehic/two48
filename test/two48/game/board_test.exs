defmodule Two48.Game.BoardTest do
  use ExUnit.Case

  alias Two48.Game.Board

  describe "new/0" do
    test "36 tiles, 1 two, 35 zeros" do
      {:ok, board} = Board.new()

      assert length(board.tiles) == 36
      assert count(board, 0) == 35
      assert count(board, 2) == 1
    end

    test "accepts obstacles parameter" do
      {:ok, board} = Board.new(%{obstacles: 5})

      assert board.obstacles == 5
      assert count(board, "x") == 5
      assert count(board, 0) == 30
      assert count(board, 2) == 1
    end
  end

  describe "from/1" do
    test "list needs to be square" do
      assert {:error, reason} = Board.from([2, 2, 2])
      assert reason =~ "bad size"
    end

    test "list needs to have valid values" do
      assert {:error, reason} = Board.from([2, 3, 2, 2])
      assert reason =~ "values not valid"
    end

    test "board can be of any size" do
      assert {:ok, board} = Board.from([2, 4, 0, 2])
      assert board.size == 2
      assert {:ok, board} = Board.from([2, 4, 0, 2, 0, 0, 0, 0, 2])
      assert board.size == 3
    end

    test "a grid can be passed as input" do
      assert {:ok, board} =
               Board.from([
                 [2, 4, 0, 2, 8, 0],
                 [2, 4, 0, 2, 8, 0],
                 [2, 4, 0, 2, 8, 0],
                 [2, 4, 0, 2, 8, 0],
                 [2, 4, 0, 2, 8, 0],
                 [2, 4, 0, 2, 8, 0]
               ])

      assert board.size == 6
    end

    test "accepts :columns optional parameter" do
      {:ok, board} =
        Board.from([
          [2, 4, 0, 2, 8, 0],
          [2, 4, 0, 2, 8, 0],
          [2, 4, 0, 2, 8, 0],
          [2, 4, 0, 2, 8, 0],
          [2, 4, 0, 2, 8, 0],
          [2, 4, 0, 2, 8, 0]
        ])

      {:ok, column_board} =
        Board.from(
          [
            [2, 2, 2, 2, 2, 2],
            [4, 4, 4, 4, 4, 4],
            [0, 0, 0, 0, 0, 0],
            [2, 2, 2, 2, 2, 2],
            [8, 8, 8, 8, 8, 8],
            [0, 0, 0, 0, 0, 0]
          ],
          columns: true
        )

      assert board == column_board
    end

    test "does not mind obstacles" do
      assert {:ok, board} = Board.from([2, 0, 0, "x"])
      assert board.obstacles == 1
    end

    test "does not mind obstacles with columns" do
      assert {:ok, _board} = Board.from([[2, 0], [0, "x"]], columns: true)
    end
  end

  describe "move/2" do
    test "moves the tiles on the board and adds a new one" do
      {:ok, board} =
        Board.from([
          [0, 0, 0],
          [0, 4, 2],
          [4, 0, 0]
        ])

      {:ok, moved} = Board.move(board, :up)

      assert count(moved, 0) == count(board, 0) - 1
      assert Enum.take(moved.tiles, 3) == [4, 4, 2]
    end

    test "win condition is calculated after move" do
      {:ok, board} = Board.from([[1024, 1024, 0], [0, 4, 2], [4, 0, 0]])
      refute board.won

      {:ok, moved} = Board.move(board, :left)
      assert moved.won
    end

    test "new board is not in a win condition" do
      {:ok, board} = Board.new()
      refute board.won
    end

    test "lose condition is calculated after move" do
      {:ok, board} = Board.from([[2, 4, 8], [2, 4, 8], [2, 4, 0]])
      refute board.lost

      {:ok, moved} = Board.move(board, :right)
      assert moved.lost
    end

    test "new board is not in a lose condition" do
      {:ok, board} = Board.new()
      refute board.lost
    end

    test "returns error if the board is not movable in direction" do
      {:ok, board} =
        Board.from([
          [4, 0, 0],
          [4, 0, 0],
          [4, 0, 0]
        ])

      assert {:error, reason} = Board.move(board, :left)
      assert reason =~ "direction"
      assert {:ok, _moved} = Board.move(board, :right)
    end

    test "won game is no longer moved" do
      {:ok, board} = Board.from([[2048, 0, 0], [4, 0, 0], [4, 0, 0]])
      board = %{board | won: true}

      assert {:error, reason} = Board.move(board, :up)
      assert reason =~ "already won"
    end

    test "lost game is no longer moved" do
      {:ok, board} = Board.from([[2, 2, 2], [4, 0, 0], [4, 0, 0]])
      board = %{board | lost: true}

      assert {:error, reason} = Board.move(board, :up)
      assert reason =~ "already lost"
    end
  end

  describe "new_tile/1" do
    test "adds a new tile" do
      {:ok, board} = Board.from([2, 4, 0, 2])
      board = Board.new_tile(board)
      assert board.tiles == [2, 4, 2, 2]
    end

    test "adds a new tile at the end" do
      {:ok, board} = Board.from([2, 4, 2, 0])
      board = Board.new_tile(board)
      assert board.tiles == [2, 4, 2, 2]
    end

    test "adds a new tile at the head" do
      {:ok, board} = Board.from([0, 4, 2, 2])
      board = Board.new_tile(board)
      assert board.tiles == [2, 4, 2, 2]
    end

    test "adds a new tile to an empty board" do
      {:ok, board} = Board.from([0, 0, 0, 0])
      board = Board.new_tile(board)

      assert count(board, 0) == 3
      assert count(board, 2) == 1
    end

    test "when the board is full return the board" do
      {:ok, board} = Board.from([8, 8, 8, 8])

      assert Board.new_tile(board) == board
    end
  end

  describe "split_by_rows/1" do
    test "chunks the tiles into rows" do
      {:ok, board} = Board.from([0, 2, 4, 2, 4, 8, 4, 8, 0])
      board = Board.split_by_rows(board)

      assert board.tiles == [
               [0, 2, 4],
               [2, 4, 8],
               [4, 8, 0]
             ]
    end
  end

  describe "split_by_columns/1" do
    test "chunks the tiles into columns" do
      {:ok, board} = Board.from([0, 2, 4, 0, 2, 4, 0, 2, 0])
      board = Board.split_by_columns(board)

      assert board.tiles == [
               [0, 0, 0],
               [2, 2, 2],
               [4, 4, 0]
             ]
    end
  end

  describe "split_by_direction/2" do
    setup do
      {:ok, board} =
        Board.from([
          [0, 2, 4],
          [8, 0, 2],
          [4, 8, 0]
        ])

      {:ok, board_with_obstacles} =
        Board.from([
          [0, 2, 4],
          [8, "x", 2],
          [4, 8, 0]
        ])

      %{board: board, board_with_obstacles: board_with_obstacles}
    end

    test "works in left direction", %{board: board} do
      split = Board.split_by_direction(board, :left)

      assert split.tiles == [
               [0, 2, 4],
               [8, 0, 2],
               [4, 8, 0]
             ]
    end

    test "works in right direction", %{board: board} do
      split = Board.split_by_direction(board, :right)

      assert split.tiles == [
               [4, 2, 0],
               [2, 0, 8],
               [0, 8, 4]
             ]
    end

    test "works in up direction", %{board: board} do
      split = Board.split_by_direction(board, :up)

      assert split.tiles == [
               [0, 8, 4],
               [2, 0, 8],
               [4, 2, 0]
             ]
    end

    test "works in down direction", %{board: board} do
      split = Board.split_by_direction(board, :down)

      assert split.tiles == [
               [4, 8, 0],
               [8, 0, 2],
               [0, 2, 4]
             ]
    end

    test "works in left direction with obstacles", %{board_with_obstacles: board} do
      split = Board.split_by_direction(board, :left)

      assert split.tiles == [
               [0, 2, 4],
               [8],
               ["x"],
               [2],
               [4, 8, 0]
             ]
    end

    test "works in right direction with obstacles", %{board_with_obstacles: board} do
      split = Board.split_by_direction(board, :right)

      assert split.tiles == [
               [4, 2, 0],
               [2],
               ["x"],
               [8],
               [0, 8, 4]
             ]
    end

    test "works in up direction with obstacles", %{board_with_obstacles: board} do
      split = Board.split_by_direction(board, :up)

      assert split.tiles == [
               [0, 8, 4],
               [2],
               ["x"],
               [8],
               [4, 2, 0]
             ]
    end

    test "works in down direction with obstacles", %{board_with_obstacles: board} do
      split = Board.split_by_direction(board, :down)

      assert split.tiles == [
               [4, 8, 0],
               [8],
               ["x"],
               [2],
               [0, 2, 4]
             ]
    end
  end

  describe "collapse/2" do
    setup do
      {:ok, board} =
        Board.from([
          [2, 2, 0],
          [2, 2, 4],
          [4, 0, 0]
        ])

      %{board: board}
    end

    test "collapses going left", %{board: board} do
      {:ok, left} =
        Board.from([
          [4, 0, 0],
          [4, 4, 0],
          [4, 0, 0]
        ])

      assert Board.collapse(board, :left) == left
    end

    test "collapsed going right", %{board: board} do
      {:ok, right} =
        Board.from([
          [0, 0, 4],
          [0, 4, 4],
          [0, 0, 4]
        ])

      assert Board.collapse(board, :right) == right
    end

    test "collapsed going up", %{board: board} do
      {:ok, up} =
        Board.from([
          [4, 4, 4],
          [4, 0, 0],
          [0, 0, 0]
        ])

      assert Board.collapse(board, :up) == up
    end

    test "collapsed going down", %{board: board} do
      {:ok, down} =
        Board.from([
          [0, 0, 0],
          [4, 0, 0],
          [4, 4, 4]
        ])

      assert Board.collapse(board, :down) == down
    end
  end

  describe "collapse with obstacles" do
    setup do
      {:ok, board} =
        Board.from([
          ["x", 0, 0, 0],
          [2, 2, "x", 2],
          [2, 2, 0, 0],
          [0, "x", "x", 0]
        ])

      %{board: board}
    end

    test "collapse left", %{board: board} do
      {:ok, collapsed} =
        Board.from([
          ["x", 0, 0, 0],
          [4, 0, "x", 2],
          [4, 0, 0, 0],
          [0, "x", "x", 0]
        ])

      assert Board.collapse(board, :left) == collapsed
    end

    test "collapse right", %{board: board} do
      {:ok, collapsed} =
        Board.from([
          ["x", 0, 0, 0],
          [0, 4, "x", 2],
          [0, 0, 0, 4],
          [0, "x", "x", 0]
        ])

      assert Board.collapse(board, :right) == collapsed
    end

    test "collapse up", %{board: board} do
      {:ok, collapsed} =
        Board.from([
          ["x", 4, 0, 2],
          [4, 0, "x", 0],
          [0, 0, 0, 0],
          [0, "x", "x", 0]
        ])

      assert Board.collapse(board, :up) == collapsed
    end

    test "collapse down", %{board: board} do
      {:ok, collapsed} =
        Board.from([
          ["x", 0, 0, 0],
          [0, 0, "x", 0],
          [0, 4, 0, 0],
          [4, "x", "x", 2]
        ])

      assert Board.collapse(board, :down) == collapsed
    end
  end

  def count(board, value) do
    Enum.filter(board.tiles, fn t -> t == value end) |> length()
  end
end
