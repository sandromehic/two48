defmodule Two48.Game.BoardUtilsTest do
  use ExUnit.Case

  alias Two48.Game.BoardUtils

  describe "members?/2" do
    test "empty data returns error" do
      assert {:error, _reason} = BoardUtils.members?([], [1, 2, 3])
    end

    test "all the elements must be in values" do
      data = [1, 2, 3, 1, 2, 3]
      values = [1, 2, 3, 4]
      assert {:ok, ^data} = BoardUtils.members?(data, values)
    end

    test "even one different element returns error" do
      data = [0, 1, 2, 3]
      values = [1, 2, 3]
      assert {:error, _reason} = BoardUtils.members?(data, values)
    end
  end

  describe "square_size/1" do
    test "return error for an empty list" do
      assert {:error, _reason} = BoardUtils.square_size([])
    end

    test "returns error if the list size is not square" do
      assert {:error, _reason} = BoardUtils.square_size([1, 2, 3])
    end

    test "returns the correct size" do
      assert {:ok, 2} = BoardUtils.square_size([1, 2, 3, 4])
      assert {:ok, 3} = BoardUtils.square_size([1, 2, 3, 4, 5, 6, 7, 8, 9])
    end
  end

  describe "square_size!/1" do
    test "raises ArgumentError on bad size" do
      assert_raise ArgumentError, fn ->
        BoardUtils.square_size!([1, 2, 3])
      end
    end

    test "returns the correct size" do
      assert BoardUtils.square_size!([1, 2, 3, 4]) == 2
    end
  end

  describe "transpose/1" do
    test "returns an error if the list is not square" do
      assert {:error, _reason} = BoardUtils.transpose([1, 2, 3])
    end

    test "transposes the list correctly" do
      assert {:ok, [1, 3, 2, 4]} = BoardUtils.transpose([1, 2, 3, 4])
    end
  end

  describe "transpose!/1" do
    test "raises ArgumentError if list is not square" do
      assert_raise ArgumentError, fn ->
        BoardUtils.transpose!([1, 2, 3])
      end
    end

    test "given a list rappresenting a matrix returns its transpose" do
      input = [0, 1, 2, 3, 4, 5, 6, 7, 8]
      output = [0, 3, 6, 1, 4, 7, 2, 5, 8]

      assert BoardUtils.transpose!(input) == output
    end
  end

  describe "collapse/1" do
    test "slides the value if there are no possible sums" do
      assert BoardUtils.collapse([0, 2, 4]) == [2, 4, 0]
      assert BoardUtils.collapse([0, 0, 8]) == [8, 0, 0]
      assert BoardUtils.collapse([4, 2, 4]) == [4, 2, 4]
      assert BoardUtils.collapse([0, 0, 8, 0, 16]) == [8, 16, 0, 0, 0]
    end

    test "sums the tiles that are next to each other and with the same value" do
      assert BoardUtils.collapse([2, 2, 0]) == [4, 0, 0]
      assert BoardUtils.collapse([2, 2, 2]) == [4, 2, 0]
      assert BoardUtils.collapse([2, 2, 4]) == [4, 4, 0]
      assert BoardUtils.collapse([0, 0, 8, 8, 16, 0]) == [16, 16, 0, 0, 0, 0]
    end
  end

  describe "disect/2" do
    test "returns an empty list if no data is given" do
      assert BoardUtils.disect([], "x") == []
    end

    test "splits the data into lists based on value" do
      input = [2, 4, "x", 2, 0, 0]
      output = [[2, 4], ["x"], [2, 0, 0]]

      assert BoardUtils.disect(input, "x") == output
    end

    test "works with multiple obstacles" do
      input = [2, 4, "x", 2, "x", 0]
      output = [[2, 4], ["x"], [2], ["x"], [0]]

      assert BoardUtils.disect(input, "x") == output
    end

    test "works with obstacles at start" do
      input = ["x", 4, "x", 2, "x", 0]
      output = [["x"], [4], ["x"], [2], ["x"], [0]]

      assert BoardUtils.disect(input, "x") == output
    end

    test "works with obstacles at end" do
      input = [2, 4, "x", 2, 0, "x"]
      output = [[2, 4], ["x"], [2, 0], ["x"]]

      assert BoardUtils.disect(input, "x") == output
    end
  end

  describe "disect_reverse/1" do
    test "returns the accumulator without reversing it" do
      input = [2, 4, "x", 2, 0, "x"]
      output = [[2, 4], ["x"], [2, 0], ["x"]] |> Enum.reverse()

      assert BoardUtils.disect_reverse(input, "x") == output
    end
  end
end
