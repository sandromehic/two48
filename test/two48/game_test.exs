defmodule Two48.GameTest do
  use ExUnit.Case

  alias Two48.Game
  alias Two48.Game.Board

  setup do
    {:ok, board} = Board.from([0, 2, 4, 0, 4, 8, 2, 0, 2])
    %{board: board}
  end

  describe "board/0" do
    test "returns a board" do
      {:ok, pid} = Game.start_link(name: :testing)

      assert %Board{} = Game.board(pid)
    end
  end

  describe "move/1" do
    test "the board is not the same after move", %{board: board} do
      {:ok, pid} = Game.start_link(name: :testing, board: board)

      moved = Game.move(pid, :up)

      refute moved == board
    end
  end

  describe "reset/1" do
    test "resets the board to a new one", %{board: board} do
      {:ok, pid} = Game.start_link(name: :testing, board: board)

      reset = Game.reset(pid)

      refute reset == board
    end
  end
end
