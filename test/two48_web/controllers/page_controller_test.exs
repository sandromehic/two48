defmodule Two48Web.PageControllerTest do
  use Two48Web.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "vue-app"
  end
end
